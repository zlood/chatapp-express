const expect = require('expect');
const {isRealString} = require('./validation');

describe('isRealString', () => {
  it('should reject non-string values', () => {
    const name = 123;
    const room = 099009;

    expect(isRealString(name)).toBeFalsy();
    expect(isRealString(room)).toBeFalsy();
  });

  it('should reject string with only spaces', () => {
    const name = '    ';
    const room = '   ';

    expect(isRealString(name)).toBeFalsy();
    expect(isRealString(room)).toBeFalsy();
  });

  it('should allow string with non-space characters', () => {
    const name = '    Junda  ';
    const room = '   node course    ';

    expect(isRealString(name)).toBeTruthy();
    expect(isRealString(room)).toBeTruthy();
  })
})