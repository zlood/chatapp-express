const _ = require('lodash')

class Users {
  constructor () {
    this.users = [];
  }

  addUser (id, name, room) {
    const user = {id, name, room};
    this.users.push(user);
  }

  removeUser (id) {
    const delUser = _.find(this.users, {id});
    const users = _.remove(this.users, (i) => {
      return i.id === id;
    });
    
    return delUser;
  }

  getUser (id) {
    const user = _.find(this.users, {id});
    return user;
  }

  getUserList (room) {
    const users = this.users.filter((user) => user.room === room);
    const namesArray = users.map((user) => user.name);

    return namesArray;
  }
}

module.exports = {Users};