const {Users} = require('./users');
const expect = require('expect');

describe('addUser', () => {
  let users;
  beforeEach(() => {
    users = new Users();
    users.users = [
      {
        id: '1',
        name: 'mike',
        room: 'Node Fans'
      }, {
        id: '2',
        name: 'jen',
        room: 'Node Fans'
      }, {
        id: '3',
        name: 'ben',
        room: 'Node Course'
      }
    ]
  });

  it('should add new user', () => {
    let users = new Users();
    let user = {
      id: '123',
      name: 'Junda',
      room: 'Node Fans'
    };
    let resUser = users.addUser(user.id, user.name, user.room);
    expect(users.users).toEqual([user]);
  });

  it('should remove a user', () => {
    let delUser = users.removeUser('1');
    let user = {
      id: '1',
      name: 'mike',
      room: 'Node Fans'
    }
    expect(delUser).toEqual(user);
    expect(users.users).toNotContain(delUser);
  });

  it('should not remove a user with invalid ID', () => {
    let delUser = users.removeUser('asdf');
    expect(delUser).toBeA('undefined');
  });

  it('should find a user', () => {
    let user = users.getUser('1');
    expect(user).toEqual(users.users[0]);
  });

  it('should find a user with invalid ID', () => {
    let user = users.getUser('qwer');
    expect(user).toBeA('undefined');
  });

  it('should return names for Node Course', () => {
    const userList = users.getUserList('Node Course');
    expect(userList).toEqual(['ben']);
  });

  it('should return names for Node Course', () => {
    const userList = users.getUserList('Node Fans');
    expect(userList).toEqual(['mike', 'jen']);
  });
})