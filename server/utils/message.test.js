const expect = require('expect');
const {generateMessage, generateLocationMessage} = require('./message');

describe('generateMessage', () => {
  it('should generate correct message object', () => {
    const from = 'Origin';
    const text = 'The message goes here';
    const message = generateMessage(from, text);

    expect(message).toInclude({from, text});
    expect(message.createdAt).toBeA('number');
  });
});

describe('generateLocationMessage', () => {
  it('should generate correct location object', () => {
    const from = 'Origin';
    const lat = 1;
    const lng = 2;
    const locationMessage = generateLocationMessage(from, lat, lng);

    expect(locationMessage.createdAt).toBeA('number');
    expect(locationMessage.url).toBe(`https://www.google.com/maps?q=${lat},${lng}`)
  });
});